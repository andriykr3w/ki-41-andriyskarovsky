--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   01:38:34 12/18/2017
-- Design Name:   
-- Module Name:   D:/FPGA/AVI_lab1a/avi_mod1_tbb.vhd
-- Project Name:  AVI_lab1a
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: AVI_mod1
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY avi_mod1_tbb IS
END avi_mod1_tbb;
 
ARCHITECTURE behavior OF avi_mod1_tbb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT AVI_mod1
    PORT(
         avi_a : IN  std_logic;
         avi_b : IN  std_logic;
         avi_c : IN  std_logic;
         avi_d : IN  std_logic;
         avi_e : IN  std_logic;
         avi_res : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal avi_a : std_logic := '0';
   signal avi_b : std_logic := '0';
   signal avi_c : std_logic := '0';
   signal avi_d : std_logic := '0';
   signal avi_e : std_logic := '0';

 	--Outputs
   signal avi_res : std_logic;
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
   constant <clock>_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: AVI_mod1 PORT MAP (
          avi_a => avi_a,
          avi_b => avi_b,
          avi_c => avi_c,
          avi_d => avi_d,
          avi_e => avi_e,
          avi_res => avi_res
        );

   -- Clock process definitions
   <clock>_process :process
   begin
		<clock> <= '0';
		wait for <clock>_period/2;
		<clock> <= '1';
		wait for <clock>_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for <clock>_period*10;

      -- insert stimulus here 

      wait;
   end process;

END;
