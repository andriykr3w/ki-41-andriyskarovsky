<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="virtex4" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_1" />
        <signal name="XLXN_2" />
        <signal name="XLXN_3" />
        <signal name="XLXN_4" />
        <signal name="XLXN_5" />
        <signal name="XLXN_8" />
        <signal name="avi_c" />
        <signal name="avi_b" />
        <signal name="avi_a" />
        <signal name="avi_d" />
        <signal name="avi_e" />
        <signal name="avi_res" />
        <signal name="XLXN_26" />
        <port polarity="Input" name="avi_c" />
        <port polarity="Input" name="avi_b" />
        <port polarity="Input" name="avi_a" />
        <port polarity="Input" name="avi_d" />
        <port polarity="Input" name="avi_e" />
        <port polarity="Output" name="avi_res" />
        <blockdef name="or2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="xor2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="60" y1="-128" y2="-128" x1="0" />
            <line x2="208" y1="-96" y2="-96" x1="256" />
            <arc ex="44" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <arc ex="64" ey="-144" sx="64" sy="-48" r="56" cx="32" cy="-96" />
            <line x2="64" y1="-144" y2="-144" x1="128" />
            <line x2="64" y1="-48" y2="-48" x1="128" />
            <arc ex="128" ey="-144" sx="208" sy="-96" r="88" cx="132" cy="-56" />
            <arc ex="208" ey="-96" sx="128" sy="-48" r="88" cx="132" cy="-136" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <block symbolname="or2" name="XLXI_1">
            <blockpin signalname="avi_b" name="I0" />
            <blockpin signalname="avi_a" name="I1" />
            <blockpin signalname="XLXN_1" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_2">
            <blockpin signalname="XLXN_4" name="I0" />
            <blockpin signalname="XLXN_3" name="I1" />
            <blockpin signalname="avi_res" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_3">
            <blockpin signalname="avi_e" name="I0" />
            <blockpin signalname="avi_d" name="I1" />
            <blockpin signalname="XLXN_4" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_4">
            <blockpin signalname="XLXN_8" name="I0" />
            <blockpin signalname="XLXN_2" name="I1" />
            <blockpin signalname="XLXN_3" name="O" />
        </block>
        <block symbolname="xor2" name="XLXI_5">
            <blockpin signalname="avi_c" name="I0" />
            <blockpin signalname="XLXN_5" name="I1" />
            <blockpin signalname="XLXN_8" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_6">
            <blockpin signalname="XLXN_1" name="I" />
            <blockpin signalname="XLXN_2" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_7">
            <blockpin signalname="avi_b" name="I" />
            <blockpin signalname="XLXN_5" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="560" y="400" name="XLXI_1" orien="R0" />
        <instance x="560" y="624" name="XLXI_7" orien="R0" />
        <instance x="576" y="1072" name="XLXI_3" orien="R0" />
        <branch name="XLXN_1">
            <wire x2="976" y1="304" y2="304" x1="816" />
        </branch>
        <branch name="XLXN_2">
            <wire x2="1392" y1="304" y2="304" x1="1200" />
        </branch>
        <branch name="XLXN_5">
            <wire x2="976" y1="592" y2="592" x1="784" />
        </branch>
        <branch name="avi_b">
            <wire x2="432" y1="336" y2="336" x1="304" />
            <wire x2="560" y1="336" y2="336" x1="432" />
            <wire x2="432" y1="336" y2="592" x1="432" />
            <wire x2="560" y1="592" y2="592" x1="432" />
        </branch>
        <branch name="avi_a">
            <wire x2="560" y1="272" y2="272" x1="304" />
        </branch>
        <branch name="avi_d">
            <wire x2="576" y1="944" y2="944" x1="336" />
        </branch>
        <branch name="avi_e">
            <wire x2="576" y1="1008" y2="1008" x1="336" />
        </branch>
        <instance x="976" y="336" name="XLXI_6" orien="R0" />
        <instance x="976" y="720" name="XLXI_5" orien="R0" />
        <branch name="avi_c">
            <wire x2="976" y1="656" y2="656" x1="320" />
        </branch>
        <instance x="1392" y="432" name="XLXI_4" orien="R0" />
        <branch name="XLXN_8">
            <wire x2="1248" y1="624" y2="624" x1="1232" />
            <wire x2="1392" y1="368" y2="368" x1="1248" />
            <wire x2="1248" y1="368" y2="624" x1="1248" />
        </branch>
        <instance x="1680" y="848" name="XLXI_2" orien="R0" />
        <branch name="XLXN_3">
            <wire x2="1664" y1="336" y2="336" x1="1648" />
            <wire x2="1664" y1="336" y2="720" x1="1664" />
            <wire x2="1680" y1="720" y2="720" x1="1664" />
        </branch>
        <branch name="XLXN_4">
            <wire x2="848" y1="976" y2="976" x1="832" />
            <wire x2="1680" y1="784" y2="784" x1="848" />
            <wire x2="848" y1="784" y2="976" x1="848" />
        </branch>
        <branch name="avi_res">
            <wire x2="2096" y1="752" y2="752" x1="1936" />
        </branch>
        <iomarker fontsize="28" x="304" y="272" name="avi_a" orien="R180" />
        <iomarker fontsize="28" x="304" y="336" name="avi_b" orien="R180" />
        <iomarker fontsize="28" x="320" y="656" name="avi_c" orien="R180" />
        <iomarker fontsize="28" x="336" y="944" name="avi_d" orien="R180" />
        <iomarker fontsize="28" x="336" y="1008" name="avi_e" orien="R180" />
        <iomarker fontsize="28" x="2096" y="752" name="avi_res" orien="R0" />
    </sheet>
</drawing>