-- Vhdl test bench created from schematic D:\FPGA\AVI_lab1a\AVI_sch1a.sch - Mon Dec 18 01:01:42 2017
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY AVI_sch1a_AVI_sch1a_sch_tb IS
END AVI_sch1a_AVI_sch1a_sch_tb;
ARCHITECTURE behavioral OF AVI_sch1a_AVI_sch1a_sch_tb IS 

   COMPONENT AVI_sch1a
   PORT( avi_c	:	IN	STD_LOGIC; 
          avi_b	:	IN	STD_LOGIC; 
          avi_a	:	IN	STD_LOGIC; 
          avi_d	:	IN	STD_LOGIC; 
          avi_e	:	IN	STD_LOGIC; 
          avi_res	:	OUT	STD_LOGIC);
   END COMPONENT;

   SIGNAL avi_c	:	STD_LOGIC:='0';
   SIGNAL avi_b	:	STD_LOGIC:='0';
   SIGNAL avi_a	:	STD_LOGIC:='0';
   SIGNAL avi_d	:	STD_LOGIC:='0';
   SIGNAL avi_e	:	STD_LOGIC:='0';
   SIGNAL avi_res	:	STD_LOGIC;

BEGIN

   UUT: AVI_sch1a PORT MAP(
		avi_c => avi_c, 
		avi_b => avi_b, 
		avi_a => avi_a, 
		avi_d => avi_d, 
		avi_e => avi_e, 
		avi_res => avi_res
   );
	avi_a<=not avi_a after 10 ns;
	avi_b<=not avi_b after 20 ns;
	avi_c<=not avi_c after 40 ns;
	avi_d<=not avi_d after 80 ns;
	avi_e<=not avi_e after 160 ns;

-- *** Test Bench - User Defined Section ***
   tb : PROCESS
   BEGIN
      WAIT; -- will wait forever
   END PROCESS;
-- *** End Test Bench - User Defined Section ***

END;
