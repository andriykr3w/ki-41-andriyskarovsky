The following files were generated for 'MTR_IP_FILTER' in directory
C:\Users\Taras\Desktop\MTR_Lab5\MTR_Lab5\ipcore_dir\

Opens the IP Customization GUI:
   Allows the user to customize or recustomize the IP instance.

   * MTR_IP_FILTER.mif

XCO file generator:
   Generate an XCO file for compatibility with legacy flows.

   * MTR_IP_FILTER.xco

Creates an implementation netlist:
   Creates an implementation netlist for the IP.

   * MTR_IP_FILTER.ngc
   * MTR_IP_FILTER.vhd
   * MTR_IP_FILTER.vho
   * MTR_IP_FILTERCOEFF_auto0_0.mif
   * MTR_IP_FILTERCOEFF_auto0_1.mif
   * MTR_IP_FILTERCOEFF_auto0_2.mif
   * MTR_IP_FILTERCOEFF_auto0_3.mif
   * MTR_IP_FILTERCOEFF_auto0_4.mif
   * MTR_IP_FILTERCOEFF_auto0_5.mif
   * MTR_IP_FILTERCOEFF_auto0_6.mif
   * MTR_IP_FILTERfilt_decode_rom.mif

Creates an HDL instantiation template:
   Creates an HDL instantiation template for the IP.

   * MTR_IP_FILTER.vho

IP Symbol Generator:
   Generate an IP symbol based on the current project options'.

   * MTR_IP_FILTER.asy
   * MTR_IP_FILTER.mif

SYM file generator:
   Generate a SYM file for compatibility with legacy flows

   * MTR_IP_FILTER.sym

Generate ISE metadata:
   Create a metadata file for use when including this core in ISE designs

   * MTR_IP_FILTER_xmdf.tcl

Generate ISE subproject:
   Create an ISE subproject for use when including this core in ISE designs

   * MTR_IP_FILTER.gise
   * MTR_IP_FILTER.xise
   * _xmsgs/pn_parser.xmsgs

Deliver Readme:
   Readme file for the IP.

   * MTR_IP_FILTER_readme.txt

Generate FLIST file:
   Text file listing all of the output files produced when a customized core was
   generated in the CORE Generator.

   * MTR_IP_FILTER_flist.txt

Please see the Xilinx CORE Generator online help for further details on
generated files and how to use them.

