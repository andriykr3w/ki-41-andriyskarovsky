<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="virtex4" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_4(7:0)" />
        <signal name="XLXN_6(7:0)" />
        <signal name="XLXN_7(7:0)" />
        <signal name="XLXN_8(7:0)" />
        <signal name="XLXN_9(7:0)" />
        <signal name="XLXN_41" />
        <signal name="XLXN_48(15:0)" />
        <signal name="XLXN_49(15:0)" />
        <signal name="XLXN_50(15:0)" />
        <signal name="XLXN_51(15:0)" />
        <signal name="XLXN_56(15:0)" />
        <signal name="XLXN_64(15:0)" />
        <signal name="XLXN_67(15:0)" />
        <signal name="XLXN_68(15:0)" />
        <signal name="XLXN_69(15:0)" />
        <signal name="XLXN_70(15:0)" />
        <signal name="XLXN_71(15:0)" />
        <signal name="XLXN_72" />
        <signal name="XLXN_73" />
        <signal name="XLXN_74" />
        <signal name="XLXN_75" />
        <signal name="XLXN_76" />
        <signal name="XLXN_77(15:0)" />
        <signal name="MTR_F(15:0)" />
        <signal name="XLXN_63(15:0)" />
        <signal name="MTR_A(7:0)" />
        <signal name="XLXN_5(7:0)" />
        <signal name="CLK" />
        <signal name="XLXN_107" />
        <signal name="CE" />
        <signal name="XLXN_111" />
        <signal name="CLR" />
        <signal name="XLXN_116" />
        <signal name="XLXN_117" />
        <signal name="XLXN_118" />
        <signal name="XLXN_119" />
        <signal name="XLXN_120" />
        <signal name="XLXN_121" />
        <port polarity="Output" name="MTR_F(15:0)" />
        <port polarity="Input" name="MTR_A(7:0)" />
        <port polarity="Input" name="CLK" />
        <port polarity="Input" name="CE" />
        <port polarity="Input" name="CLR" />
        <blockdef name="MTR_MUL_111">
            <timestamp>2016-11-27T12:58:13</timestamp>
            <rect width="320" x="64" y="-64" height="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="384" y="-44" height="24" />
            <line x2="448" y1="-32" y2="-32" x1="384" />
        </blockdef>
        <blockdef name="MTR_MUL_92">
            <timestamp>2016-11-27T12:58:17</timestamp>
            <rect width="320" x="64" y="-64" height="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="384" y="-44" height="24" />
            <line x2="448" y1="-32" y2="-32" x1="384" />
        </blockdef>
        <blockdef name="fd8ce">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="64" y1="-32" y2="-32" x1="192" />
            <line x2="192" y1="-64" y2="-32" x1="192" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <rect width="64" x="320" y="-268" height="24" />
            <rect width="64" x="0" y="-268" height="24" />
            <rect width="256" x="64" y="-320" height="256" />
        </blockdef>
        <blockdef name="add16">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="384" y1="-128" y2="-128" x1="448" />
            <line x2="384" y1="-64" y2="-64" x1="448" />
            <line x2="384" y1="-64" y2="-64" x1="240" />
            <line x2="240" y1="-124" y2="-64" x1="240" />
            <rect width="64" x="0" y="-204" height="24" />
            <rect width="64" x="0" y="-332" height="24" />
            <line x2="64" y1="-320" y2="-320" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="384" y1="-256" y2="-256" x1="448" />
            <rect width="64" x="384" y="-268" height="24" />
            <line x2="64" y1="-448" y2="-448" x1="0" />
            <line x2="64" y1="-448" y2="-448" x1="128" />
            <line x2="128" y1="-416" y2="-448" x1="128" />
            <line x2="64" y1="-288" y2="-432" x1="64" />
            <line x2="64" y1="-256" y2="-288" x1="128" />
            <line x2="128" y1="-224" y2="-256" x1="64" />
            <line x2="64" y1="-80" y2="-224" x1="64" />
            <line x2="64" y1="-160" y2="-80" x1="384" />
            <line x2="384" y1="-336" y2="-160" x1="384" />
            <line x2="384" y1="-352" y2="-336" x1="384" />
            <line x2="384" y1="-432" y2="-352" x1="64" />
            <line x2="336" y1="-128" y2="-148" x1="336" />
            <line x2="336" y1="-128" y2="-128" x1="384" />
        </blockdef>
        <blockdef name="gnd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-96" x1="64" />
            <line x2="52" y1="-48" y2="-48" x1="76" />
            <line x2="60" y1="-32" y2="-32" x1="68" />
            <line x2="40" y1="-64" y2="-64" x1="88" />
            <line x2="64" y1="-64" y2="-80" x1="64" />
            <line x2="64" y1="-128" y2="-96" x1="64" />
        </blockdef>
        <blockdef name="fd16ce">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <rect width="64" x="320" y="-268" height="24" />
            <rect width="64" x="0" y="-268" height="24" />
            <line x2="64" y1="-32" y2="-32" x1="192" />
            <line x2="192" y1="-64" y2="-32" x1="192" />
            <rect width="256" x="64" y="-320" height="256" />
        </blockdef>
        <block symbolname="fd8ce" name="XLXI_9">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="MTR_A(7:0)" name="D(7:0)" />
            <blockpin signalname="XLXN_4(7:0)" name="Q(7:0)" />
        </block>
        <block symbolname="fd8ce" name="XLXI_8">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="XLXN_4(7:0)" name="D(7:0)" />
            <blockpin signalname="XLXN_5(7:0)" name="Q(7:0)" />
        </block>
        <block symbolname="MTR_MUL_111" name="XLXI_1">
            <blockpin signalname="XLXN_4(7:0)" name="MTR_A(7:0)" />
            <blockpin signalname="XLXN_64(15:0)" name="MTR_OPT(15:0)" />
        </block>
        <block symbolname="MTR_MUL_92" name="XLXI_4">
            <blockpin signalname="XLXN_6(7:0)" name="MTR_A(7:0)" />
            <blockpin signalname="XLXN_68(15:0)" name="MTR_OPT(15:0)" />
        </block>
        <block symbolname="fd8ce" name="XLXI_11">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="XLXN_6(7:0)" name="D(7:0)" />
            <blockpin signalname="XLXN_7(7:0)" name="Q(7:0)" />
        </block>
        <block symbolname="MTR_MUL_92" name="XLXI_5">
            <blockpin signalname="XLXN_7(7:0)" name="MTR_A(7:0)" />
            <blockpin signalname="XLXN_69(15:0)" name="MTR_OPT(15:0)" />
        </block>
        <block symbolname="fd8ce" name="XLXI_12">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="XLXN_7(7:0)" name="D(7:0)" />
            <blockpin signalname="XLXN_8(7:0)" name="Q(7:0)" />
        </block>
        <block symbolname="MTR_MUL_92" name="XLXI_6">
            <blockpin signalname="XLXN_8(7:0)" name="MTR_A(7:0)" />
            <blockpin signalname="XLXN_70(15:0)" name="MTR_OPT(15:0)" />
        </block>
        <block symbolname="fd8ce" name="XLXI_13">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="XLXN_8(7:0)" name="D(7:0)" />
            <blockpin signalname="XLXN_9(7:0)" name="Q(7:0)" />
        </block>
        <block symbolname="MTR_MUL_92" name="XLXI_7">
            <blockpin signalname="XLXN_9(7:0)" name="MTR_A(7:0)" />
            <blockpin signalname="XLXN_71(15:0)" name="MTR_OPT(15:0)" />
        </block>
        <block symbolname="add16" name="XLXI_111">
            <blockpin signalname="XLXN_63(15:0)" name="A(15:0)" />
            <blockpin signalname="XLXN_64(15:0)" name="B(15:0)" />
            <blockpin signalname="XLXN_41" name="CI" />
            <blockpin signalname="XLXN_72" name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="XLXN_48(15:0)" name="S(15:0)" />
        </block>
        <block symbolname="add16" name="XLXI_112">
            <blockpin signalname="XLXN_67(15:0)" name="A(15:0)" />
            <blockpin signalname="XLXN_48(15:0)" name="B(15:0)" />
            <blockpin signalname="XLXN_72" name="CI" />
            <blockpin signalname="XLXN_73" name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="XLXN_49(15:0)" name="S(15:0)" />
        </block>
        <block symbolname="add16" name="XLXI_113">
            <blockpin signalname="XLXN_68(15:0)" name="A(15:0)" />
            <blockpin signalname="XLXN_49(15:0)" name="B(15:0)" />
            <blockpin signalname="XLXN_73" name="CI" />
            <blockpin signalname="XLXN_74" name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="XLXN_50(15:0)" name="S(15:0)" />
        </block>
        <block symbolname="add16" name="XLXI_114">
            <blockpin signalname="XLXN_69(15:0)" name="A(15:0)" />
            <blockpin signalname="XLXN_50(15:0)" name="B(15:0)" />
            <blockpin signalname="XLXN_74" name="CI" />
            <blockpin signalname="XLXN_75" name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="XLXN_51(15:0)" name="S(15:0)" />
        </block>
        <block symbolname="add16" name="XLXI_115">
            <blockpin signalname="XLXN_70(15:0)" name="A(15:0)" />
            <blockpin signalname="XLXN_51(15:0)" name="B(15:0)" />
            <blockpin signalname="XLXN_75" name="CI" />
            <blockpin signalname="XLXN_76" name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="XLXN_56(15:0)" name="S(15:0)" />
        </block>
        <block symbolname="add16" name="XLXI_116">
            <blockpin signalname="XLXN_71(15:0)" name="A(15:0)" />
            <blockpin signalname="XLXN_56(15:0)" name="B(15:0)" />
            <blockpin signalname="XLXN_76" name="CI" />
            <blockpin name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="XLXN_77(15:0)" name="S(15:0)" />
        </block>
        <block symbolname="gnd" name="XLXI_117">
            <blockpin signalname="XLXN_41" name="G" />
        </block>
        <block symbolname="fd16ce" name="XLXI_125">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="XLXN_77(15:0)" name="D(15:0)" />
            <blockpin signalname="MTR_F(15:0)" name="Q(15:0)" />
        </block>
        <block symbolname="MTR_MUL_111" name="XLXI_2">
            <blockpin signalname="MTR_A(7:0)" name="MTR_A(7:0)" />
            <blockpin signalname="XLXN_63(15:0)" name="MTR_OPT(15:0)" />
        </block>
        <block symbolname="MTR_MUL_111" name="XLXI_3">
            <blockpin signalname="XLXN_5(7:0)" name="MTR_A(7:0)" />
            <blockpin signalname="XLXN_67(15:0)" name="MTR_OPT(15:0)" />
        </block>
        <block symbolname="fd8ce" name="XLXI_10">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="CE" name="CE" />
            <blockpin signalname="CLR" name="CLR" />
            <blockpin signalname="XLXN_5(7:0)" name="D(7:0)" />
            <blockpin signalname="XLXN_6(7:0)" name="Q(7:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="5440" height="3520">
        <instance x="1280" y="1072" name="XLXI_8" orien="R0" />
        <instance x="1248" y="1328" name="XLXI_1" orien="R0">
        </instance>
        <instance x="2352" y="1344" name="XLXI_4" orien="R0">
        </instance>
        <instance x="2368" y="1072" name="XLXI_11" orien="R0" />
        <branch name="XLXN_6(7:0)">
            <wire x2="2336" y1="816" y2="816" x1="2192" />
            <wire x2="2368" y1="816" y2="816" x1="2336" />
            <wire x2="2336" y1="816" y2="1312" x1="2336" />
            <wire x2="2352" y1="1312" y2="1312" x1="2336" />
        </branch>
        <instance x="2976" y="1344" name="XLXI_5" orien="R0">
        </instance>
        <instance x="2944" y="1072" name="XLXI_12" orien="R0" />
        <branch name="XLXN_7(7:0)">
            <wire x2="2896" y1="816" y2="816" x1="2752" />
            <wire x2="2944" y1="816" y2="816" x1="2896" />
            <wire x2="2896" y1="816" y2="1312" x1="2896" />
            <wire x2="2976" y1="1312" y2="1312" x1="2896" />
        </branch>
        <instance x="3600" y="1344" name="XLXI_6" orien="R0">
        </instance>
        <branch name="XLXN_8(7:0)">
            <wire x2="3520" y1="816" y2="816" x1="3328" />
            <wire x2="3568" y1="816" y2="816" x1="3520" />
            <wire x2="3520" y1="816" y2="1312" x1="3520" />
            <wire x2="3600" y1="1312" y2="1312" x1="3520" />
        </branch>
        <instance x="3568" y="1072" name="XLXI_13" orien="R0" />
        <instance x="4176" y="1344" name="XLXI_7" orien="R0">
        </instance>
        <branch name="XLXN_9(7:0)">
            <wire x2="4096" y1="816" y2="816" x1="3952" />
            <wire x2="4096" y1="816" y2="1312" x1="4096" />
            <wire x2="4176" y1="1312" y2="1312" x1="4096" />
        </branch>
        <instance x="1200" y="2000" name="XLXI_111" orien="R0" />
        <instance x="1872" y="2000" name="XLXI_112" orien="R0" />
        <instance x="2432" y="1984" name="XLXI_113" orien="R0" />
        <instance x="2992" y="1984" name="XLXI_114" orien="R0" />
        <instance x="3552" y="1984" name="XLXI_115" orien="R0" />
        <instance x="4160" y="1968" name="XLXI_116" orien="R0" />
        <instance x="1056" y="1488" name="XLXI_117" orien="R90" />
        <branch name="XLXN_41">
            <wire x2="1200" y1="1552" y2="1552" x1="1184" />
        </branch>
        <branch name="XLXN_48(15:0)">
            <wire x2="1760" y1="1744" y2="1744" x1="1648" />
            <wire x2="1760" y1="1744" y2="1808" x1="1760" />
            <wire x2="1872" y1="1808" y2="1808" x1="1760" />
        </branch>
        <branch name="XLXN_49(15:0)">
            <wire x2="2368" y1="1744" y2="1744" x1="2320" />
            <wire x2="2368" y1="1744" y2="1792" x1="2368" />
            <wire x2="2432" y1="1792" y2="1792" x1="2368" />
        </branch>
        <branch name="XLXN_50(15:0)">
            <wire x2="2928" y1="1728" y2="1728" x1="2880" />
            <wire x2="2928" y1="1728" y2="1792" x1="2928" />
            <wire x2="2992" y1="1792" y2="1792" x1="2928" />
        </branch>
        <branch name="XLXN_51(15:0)">
            <wire x2="3488" y1="1728" y2="1728" x1="3440" />
            <wire x2="3488" y1="1728" y2="1792" x1="3488" />
            <wire x2="3552" y1="1792" y2="1792" x1="3488" />
        </branch>
        <branch name="XLXN_56(15:0)">
            <wire x2="4080" y1="1728" y2="1728" x1="4000" />
            <wire x2="4080" y1="1728" y2="1776" x1="4080" />
            <wire x2="4160" y1="1776" y2="1776" x1="4080" />
        </branch>
        <branch name="XLXN_64(15:0)">
            <wire x2="672" y1="1488" y2="1808" x1="672" />
            <wire x2="1200" y1="1808" y2="1808" x1="672" />
            <wire x2="1744" y1="1488" y2="1488" x1="672" />
            <wire x2="1744" y1="1296" y2="1296" x1="1696" />
            <wire x2="1744" y1="1296" y2="1488" x1="1744" />
        </branch>
        <branch name="XLXN_67(15:0)">
            <wire x2="1808" y1="1472" y2="1680" x1="1808" />
            <wire x2="1872" y1="1680" y2="1680" x1="1808" />
            <wire x2="2304" y1="1472" y2="1472" x1="1808" />
            <wire x2="2304" y1="1296" y2="1296" x1="2240" />
            <wire x2="2304" y1="1296" y2="1472" x1="2304" />
        </branch>
        <branch name="XLXN_68(15:0)">
            <wire x2="2368" y1="1472" y2="1664" x1="2368" />
            <wire x2="2432" y1="1664" y2="1664" x1="2368" />
            <wire x2="2864" y1="1472" y2="1472" x1="2368" />
            <wire x2="2864" y1="1312" y2="1312" x1="2800" />
            <wire x2="2864" y1="1312" y2="1472" x1="2864" />
        </branch>
        <branch name="XLXN_69(15:0)">
            <wire x2="2944" y1="1472" y2="1664" x1="2944" />
            <wire x2="2992" y1="1664" y2="1664" x1="2944" />
            <wire x2="3472" y1="1472" y2="1472" x1="2944" />
            <wire x2="3472" y1="1312" y2="1312" x1="3424" />
            <wire x2="3472" y1="1312" y2="1472" x1="3472" />
        </branch>
        <branch name="XLXN_70(15:0)">
            <wire x2="4064" y1="1456" y2="1456" x1="3520" />
            <wire x2="3520" y1="1456" y2="1664" x1="3520" />
            <wire x2="3552" y1="1664" y2="1664" x1="3520" />
            <wire x2="4064" y1="1312" y2="1312" x1="4048" />
            <wire x2="4064" y1="1312" y2="1456" x1="4064" />
        </branch>
        <branch name="XLXN_71(15:0)">
            <wire x2="4704" y1="1440" y2="1440" x1="4096" />
            <wire x2="4096" y1="1440" y2="1648" x1="4096" />
            <wire x2="4160" y1="1648" y2="1648" x1="4096" />
            <wire x2="4704" y1="1312" y2="1312" x1="4624" />
            <wire x2="4704" y1="1312" y2="1440" x1="4704" />
        </branch>
        <branch name="XLXN_72">
            <wire x2="1744" y1="1936" y2="1936" x1="1648" />
            <wire x2="1744" y1="1552" y2="1936" x1="1744" />
            <wire x2="1872" y1="1552" y2="1552" x1="1744" />
        </branch>
        <branch name="XLXN_73">
            <wire x2="2352" y1="1936" y2="1936" x1="2320" />
            <wire x2="2352" y1="1536" y2="1936" x1="2352" />
            <wire x2="2432" y1="1536" y2="1536" x1="2352" />
        </branch>
        <branch name="XLXN_74">
            <wire x2="2912" y1="1920" y2="1920" x1="2880" />
            <wire x2="2912" y1="1536" y2="1920" x1="2912" />
            <wire x2="2992" y1="1536" y2="1536" x1="2912" />
        </branch>
        <branch name="XLXN_75">
            <wire x2="3472" y1="1920" y2="1920" x1="3440" />
            <wire x2="3472" y1="1536" y2="1920" x1="3472" />
            <wire x2="3552" y1="1536" y2="1536" x1="3472" />
        </branch>
        <branch name="XLXN_76">
            <wire x2="4064" y1="1920" y2="1920" x1="4000" />
            <wire x2="4064" y1="1520" y2="1920" x1="4064" />
            <wire x2="4160" y1="1520" y2="1520" x1="4064" />
        </branch>
        <instance x="4224" y="2416" name="XLXI_125" orien="R0" />
        <branch name="XLXN_77(15:0)">
            <wire x2="4688" y1="2016" y2="2016" x1="4160" />
            <wire x2="4160" y1="2016" y2="2160" x1="4160" />
            <wire x2="4224" y1="2160" y2="2160" x1="4160" />
            <wire x2="4688" y1="1712" y2="1712" x1="4608" />
            <wire x2="4688" y1="1712" y2="2016" x1="4688" />
        </branch>
        <branch name="MTR_F(15:0)">
            <wire x2="4736" y1="2160" y2="2160" x1="4608" />
        </branch>
        <branch name="XLXN_63(15:0)">
            <wire x2="592" y1="1376" y2="1680" x1="592" />
            <wire x2="1200" y1="1680" y2="1680" x1="592" />
            <wire x2="1136" y1="1376" y2="1376" x1="592" />
            <wire x2="1136" y1="1296" y2="1296" x1="1088" />
            <wire x2="1136" y1="1296" y2="1376" x1="1136" />
        </branch>
        <instance x="640" y="1328" name="XLXI_2" orien="R0">
        </instance>
        <branch name="XLXN_5(7:0)">
            <wire x2="1776" y1="816" y2="816" x1="1664" />
            <wire x2="1808" y1="816" y2="816" x1="1776" />
            <wire x2="1776" y1="816" y2="1296" x1="1776" />
            <wire x2="1792" y1="1296" y2="1296" x1="1776" />
        </branch>
        <instance x="1792" y="1328" name="XLXI_3" orien="R0">
        </instance>
        <iomarker fontsize="28" x="352" y="816" name="MTR_A(7:0)" orien="R180" />
        <iomarker fontsize="28" x="4736" y="2160" name="MTR_F(15:0)" orien="R0" />
        <instance x="1808" y="1072" name="XLXI_10" orien="R0" />
        <branch name="CLK">
            <wire x2="416" y1="944" y2="944" x1="336" />
            <wire x2="672" y1="944" y2="944" x1="416" />
            <wire x2="416" y1="944" y2="2288" x1="416" />
            <wire x2="4224" y1="2288" y2="2288" x1="416" />
            <wire x2="416" y1="672" y2="944" x1="416" />
            <wire x2="1136" y1="672" y2="672" x1="416" />
            <wire x2="1136" y1="672" y2="944" x1="1136" />
            <wire x2="1280" y1="944" y2="944" x1="1136" />
            <wire x2="1728" y1="672" y2="672" x1="1136" />
            <wire x2="1728" y1="672" y2="944" x1="1728" />
            <wire x2="1808" y1="944" y2="944" x1="1728" />
            <wire x2="2256" y1="672" y2="672" x1="1728" />
            <wire x2="2256" y1="672" y2="944" x1="2256" />
            <wire x2="2368" y1="944" y2="944" x1="2256" />
            <wire x2="2816" y1="672" y2="672" x1="2256" />
            <wire x2="2816" y1="672" y2="944" x1="2816" />
            <wire x2="2944" y1="944" y2="944" x1="2816" />
            <wire x2="3392" y1="672" y2="672" x1="2816" />
            <wire x2="3392" y1="672" y2="944" x1="3392" />
            <wire x2="3568" y1="944" y2="944" x1="3392" />
        </branch>
        <iomarker fontsize="28" x="336" y="944" name="CLK" orien="R180" />
        <branch name="CE">
            <wire x2="480" y1="880" y2="880" x1="336" />
            <wire x2="672" y1="880" y2="880" x1="480" />
            <wire x2="480" y1="880" y2="2224" x1="480" />
            <wire x2="4224" y1="2224" y2="2224" x1="480" />
            <wire x2="1120" y1="592" y2="592" x1="480" />
            <wire x2="1120" y1="592" y2="880" x1="1120" />
            <wire x2="1280" y1="880" y2="880" x1="1120" />
            <wire x2="1712" y1="592" y2="592" x1="1120" />
            <wire x2="1712" y1="592" y2="880" x1="1712" />
            <wire x2="1808" y1="880" y2="880" x1="1712" />
            <wire x2="2240" y1="592" y2="592" x1="1712" />
            <wire x2="2240" y1="592" y2="880" x1="2240" />
            <wire x2="2368" y1="880" y2="880" x1="2240" />
            <wire x2="2800" y1="592" y2="592" x1="2240" />
            <wire x2="2800" y1="592" y2="880" x1="2800" />
            <wire x2="2944" y1="880" y2="880" x1="2800" />
            <wire x2="3408" y1="592" y2="592" x1="2800" />
            <wire x2="3408" y1="592" y2="880" x1="3408" />
            <wire x2="3568" y1="880" y2="880" x1="3408" />
            <wire x2="480" y1="592" y2="880" x1="480" />
        </branch>
        <iomarker fontsize="28" x="336" y="880" name="CE" orien="R180" />
        <branch name="MTR_A(7:0)">
            <wire x2="576" y1="816" y2="816" x1="352" />
            <wire x2="672" y1="816" y2="816" x1="576" />
            <wire x2="576" y1="816" y2="1296" x1="576" />
            <wire x2="640" y1="1296" y2="1296" x1="576" />
        </branch>
        <branch name="XLXN_4(7:0)">
            <wire x2="1168" y1="816" y2="816" x1="1056" />
            <wire x2="1280" y1="816" y2="816" x1="1168" />
            <wire x2="1168" y1="816" y2="1296" x1="1168" />
            <wire x2="1248" y1="1296" y2="1296" x1="1168" />
        </branch>
        <instance x="672" y="1072" name="XLXI_9" orien="R0" />
        <branch name="CLR">
            <wire x2="448" y1="1072" y2="1072" x1="352" />
            <wire x2="656" y1="1072" y2="1072" x1="448" />
            <wire x2="1280" y1="1072" y2="1072" x1="656" />
            <wire x2="1808" y1="1072" y2="1072" x1="1280" />
            <wire x2="2368" y1="1072" y2="1072" x1="1808" />
            <wire x2="2944" y1="1072" y2="1072" x1="2368" />
            <wire x2="3568" y1="1072" y2="1072" x1="2944" />
            <wire x2="448" y1="1072" y2="2384" x1="448" />
            <wire x2="4224" y1="2384" y2="2384" x1="448" />
            <wire x2="672" y1="1040" y2="1040" x1="656" />
            <wire x2="656" y1="1040" y2="1072" x1="656" />
            <wire x2="1280" y1="1040" y2="1072" x1="1280" />
            <wire x2="1808" y1="1040" y2="1072" x1="1808" />
            <wire x2="2368" y1="1040" y2="1072" x1="2368" />
            <wire x2="2944" y1="1040" y2="1072" x1="2944" />
            <wire x2="3568" y1="1040" y2="1072" x1="3568" />
        </branch>
        <iomarker fontsize="28" x="352" y="1072" name="CLR" orien="R180" />
    </sheet>
</drawing>