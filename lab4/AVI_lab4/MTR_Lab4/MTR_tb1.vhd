-- Vhdl test bench created from schematic C:\PLIS\MTR_Lab4\MTR_Lab4\MTR_Lab4\MTR_sw2.sch - Sat Dec 03 14:22:30 2016
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY MTR_sw2_MTR_sw2_sch_tb IS
END MTR_sw2_MTR_sw2_sch_tb;
ARCHITECTURE behavioral OF MTR_sw2_MTR_sw2_sch_tb IS 

   COMPONENT MTR_sw2
   PORT( mtr_a	:	IN	STD_LOGIC_VECTOR (7 DOWNTO 0); 
         mtr_b	:	IN	STD_LOGIC_VECTOR (7 DOWNTO 0); 
         mtr_c	:	IN	STD_LOGIC_VECTOR (7 DOWNTO 0); 
          res1	:	OUT	STD_LOGIC_VECTOR (31 DOWNTO 0); 
          --res2	:	OUT	STD_LOGIC_VECTOR (31 DOWNTO 0); 
          counter	:	OUT	STD_LOGIC_VECTOR (3 DOWNTO 0); 
          Control	:	OUT	STD_LOGIC_VECTOR (19 DOWNTO 0); 
          Res	:	OUT	STD_LOGIC_VECTOR (31 DOWNTO 0); 
          CE	:	IN	STD_LOGIC; 
          Zero_bit	:	IN	STD_LOGIC ; 
          CLK	:	IN	STD_LOGIC);
   END COMPONENT;

   SIGNAL mtr_a	:	STD_LOGIC_VECTOR (7 DOWNTO 0) :=x"01";
   SIGNAL mtr_b	:	STD_LOGIC_VECTOR (7 DOWNTO 0) :=x"02";
   SIGNAL mtr_c	:	STD_LOGIC_VECTOR (7 DOWNTO 0) :=x"04";
   SIGNAL res1	:	STD_LOGIC_VECTOR (31 DOWNTO 0);
   --SIGNAL res2	:	STD_LOGIC_VECTOR (31 DOWNTO 0);
   SIGNAL counter	:	STD_LOGIC_VECTOR (3 DOWNTO 0);
   SIGNAL Control	:	STD_LOGIC_VECTOR (19 DOWNTO 0);
   SIGNAL Res	:	STD_LOGIC_VECTOR (31 DOWNTO 0);
   SIGNAL CE	:	STD_LOGIC  :='1';
   SIGNAL Zero_bit	:	STD_LOGIC  :='0';
   SIGNAL CLK	:	STD_LOGIC;
	constant clk_c : time := 20 ns;

BEGIN

   UUT: MTR_sw2 PORT MAP(
		mtr_a => mtr_a, 
		mtr_b => mtr_b, 
		mtr_c => mtr_c, 
		res1 => res1, 
		--res2 => res2, 
		counter => counter, 
		Control => Control, 
		Res => Res, 
		CE => CE, 
		Zero_bit => Zero_bit, 
		CLK => CLK
   );
	CLC_process :process

	begin
	CLK <= '1';
	wait for clk_c/2;
		
	CLK <= '0';
	wait for clk_c/2;
	end process;
	

-- *** Test Bench - User Defined Section ***
   tb : PROCESS
   BEGIN
	wait for 400 ns; 
	for i in 0 to 10 loop
	--CLK <='0'; 
	--wait for 320 ns; 
	mtr_b <= std_logic_vector(unsigned(mtr_b) + (1)); 
	mtr_c <= std_logic_vector(unsigned(mtr_c) + (1)); 
	mtr_a <= std_logic_vector(unsigned(mtr_a) + (1)); 
	--CLK <= '1'; 
	wait for 320 ns;
	end loop;
      WAIT; -- will wait forever
   END PROCESS;
-- *** End Test Bench - User Defined Section ***

END;
