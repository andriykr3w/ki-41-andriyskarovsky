-- Vhdl test bench created from schematic C:\Users\Taras\Desktop\MTR_Lab4\MTR_Lab4\MTR_SCH.sch - Sun Nov 20 20:23:04 2016
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY MTR_SCH_MTR_SCH_sch_tb IS
END MTR_SCH_MTR_SCH_sch_tb;
ARCHITECTURE behavioral OF MTR_SCH_MTR_SCH_sch_tb IS 

   COMPONENT MTR_SCH
   PORT( 
			 CE	:	IN	STD_LOGIC; 
			 C	:	IN	STD_LOGIC; 
			 CLR	:	IN	STD_LOGIC;
			 
			 mtr_b	:	IN	STD_LOGIC_VECTOR (7 DOWNTO 0); 
          mtr_c	:	IN	STD_LOGIC_VECTOR (7 DOWNTO 0); 
          mtr_a	:	IN	STD_LOGIC_VECTOR (7 DOWNTO 0); 
          mtr_mul1	:	OUT	STD_LOGIC_VECTOR (15 DOWNTO 0); 
          mtr_add	:	OUT	STD_LOGIC_VECTOR (15 DOWNTO 0); 
          mtr_res	:	OUT	STD_LOGIC_VECTOR (31 DOWNTO 0));
   END COMPONENT;
	
	
   SIGNAL CE	:	STD_LOGIC := '1';
   SIGNAL C		:	STD_LOGIC := '1';
  SIGNAL CLR	:	STD_LOGIC := '0';
	SIGNAL mtr_b	:	STD_LOGIC_VECTOR (7 DOWNTO 0);
   SIGNAL mtr_c	:	STD_LOGIC_VECTOR (7 DOWNTO 0);
   SIGNAL mtr_a	:	STD_LOGIC_VECTOR (7 DOWNTO 0);
   SIGNAL mtr_mul1	:	STD_LOGIC_VECTOR (15 DOWNTO 0);
   SIGNAL mtr_add	:	STD_LOGIC_VECTOR (15 DOWNTO 0);
   SIGNAL mtr_res	:	STD_LOGIC_VECTOR (31 DOWNTO 0);
	constant clk_c : time :=10 ns;
	SIGNAL buf : integer :=0;

BEGIN

   UUT: MTR_SCH PORT MAP(
		
		CE => CE, 
		C => C, 
		CLR => CLR,
		mtr_b => mtr_b, 
		mtr_c => mtr_c, 
		mtr_a => mtr_a, 
		mtr_mul1 => mtr_mul1, 
		mtr_add => mtr_add, 
		mtr_res => mtr_res
   );

-- *** Test Bench - User Defined Section ***
   tb : PROCESS
   

   BEGIN
	
	mtr_b <= "00001000"; 
	mtr_c <= "00000110"; 
	mtr_a <= "00000100";
	C <='0'; 
    
	for i in 0 to 10 loop
	
	
	C <='0'; 
	
	buf <= buf + 1;
	
	--wait for 5 ns;
	wait for clk_c/2; 
	
	mtr_b <= std_logic_vector(unsigned(mtr_b) + (1)); 
	mtr_c <= std_logic_vector(unsigned(mtr_c) + (1)); 
	mtr_a <= std_logic_vector(unsigned(mtr_a) + (1)); 
	
	
	C <= '1'; 
	wait for clk_c/2;
	
	
	--wait for 5 ns;
	end loop;
 
   END PROCESS;
-- *** End Test Bench - User Defined Section ***

END;
