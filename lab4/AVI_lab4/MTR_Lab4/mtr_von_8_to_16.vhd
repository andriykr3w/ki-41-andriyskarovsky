----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:16:21 12/03/2016 
-- Design Name: 
-- Module Name:    mtr_von_8_to_16 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mtr_con_8_to_32 is
    Port ( s8 : in  STD_LOGIC_VECTOR (7 downto 0);
           s32 : out  STD_LOGIC_VECTOR (32 downto 0));
end mtr_con_8_to_32;

architecture Behavioral of mtr_con_8_to_32 is
begin
	s32 <= X"000000" & s8;

end Behavioral;

