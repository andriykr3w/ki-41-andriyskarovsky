-- Vhdl test bench created from schematic E:\plis\MRI_3\MRI_sch.sch - Mon Nov 14 19:40:26 2016
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY MRI_sch_MRI_sch_sch_tb IS
END MRI_sch_MRI_sch_sch_tb;
ARCHITECTURE behavioral OF MRI_sch_MRI_sch_sch_tb IS 

   COMPONENT MRI_sch
   PORT( MRI_A	:	IN	STD_LOGIC_VECTOR (7 DOWNTO 0); 
          MRI_NO	:	OUT	STD_LOGIC_VECTOR (39 DOWNTO 0); 
          MRI_OP	:	OUT	STD_LOGIC_VECTOR (39 DOWNTO 0); 
          MRI_IP	:	OUT	STD_LOGIC_VECTOR (39 DOWNTO 0));
   END COMPONENT;

   SIGNAL MRI_A	:	STD_LOGIC_VECTOR (7 DOWNTO 0);
   SIGNAL MRI_NO	:	STD_LOGIC_VECTOR (39 DOWNTO 0);
   SIGNAL MRI_OP	:	STD_LOGIC_VECTOR (39 DOWNTO 0);
   SIGNAL MRI_IP	:	STD_LOGIC_VECTOR (39 DOWNTO 0);

BEGIN

   UUT: MRI_sch PORT MAP(
		MRI_A => MRI_A, 
		MRI_NO => MRI_NO, 
		MRI_OP => MRI_OP, 
		MRI_IP => MRI_IP
   );

-- *** Test Bench - User Defined Section ***
   tb : PROCESS
   BEGIN
	MRI_A <= "00000011";
for i in 1 to 20 loop
wait for 5ns;
MRI_A <= std_logic_vector(unsigned(MRI_A) + (1));
wait for 5 ns;
end loop;

      WAIT; -- will wait forever
   END PROCESS;
-- *** End Test Bench - User Defined Section ***

END;
