/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "E:/plis/MRI_3/MRI_2.vhd";
extern char *IEEE_P_3499444699;

char *ieee_p_3499444699_sub_2254111597_3536714472(char *, char *, char *, char *, char *, char *);
char *ieee_p_3499444699_sub_2254183471_3536714472(char *, char *, char *, char *, char *, char *);


static void work_a_1475831506_3212880686_p_0(char *t0)
{
    char t5[16];
    char t7[16];
    char t15[16];
    char t17[16];
    char *t1;
    char *t3;
    char *t4;
    char *t6;
    char *t8;
    char *t9;
    int t10;
    unsigned int t11;
    char *t12;
    char *t14;
    char *t16;
    char *t18;
    char *t19;
    int t20;
    unsigned int t21;
    unsigned char t22;
    char *t23;
    char *t24;
    char *t25;
    char *t26;
    char *t27;

LAB0:    xsi_set_current_line(17, ng0);

LAB3:    t1 = (t0 + 10160);
    t3 = (t0 + 1032U);
    t4 = *((char **)t3);
    t6 = ((IEEE_P_3499444699) + 2616);
    t8 = (t7 + 0U);
    t9 = (t8 + 0U);
    *((int *)t9) = 0;
    t9 = (t8 + 4U);
    *((int *)t9) = 27;
    t9 = (t8 + 8U);
    *((int *)t9) = 1;
    t10 = (27 - 0);
    t11 = (t10 * 1);
    t11 = (t11 + 1);
    t9 = (t8 + 12U);
    *((unsigned int *)t9) = t11;
    t9 = (t0 + 9696U);
    t3 = xsi_base_array_concat(t3, t5, t6, (char)97, t1, t7, (char)97, t4, t9, (char)101);
    t12 = (t0 + 10188);
    t16 = ((IEEE_P_3499444699) + 2616);
    t18 = (t17 + 0U);
    t19 = (t18 + 0U);
    *((int *)t19) = 0;
    t19 = (t18 + 4U);
    *((int *)t19) = 3;
    t19 = (t18 + 8U);
    *((int *)t19) = 1;
    t20 = (3 - 0);
    t11 = (t20 * 1);
    t11 = (t11 + 1);
    t19 = (t18 + 12U);
    *((unsigned int *)t19) = t11;
    t14 = xsi_base_array_concat(t14, t15, t16, (char)97, t3, t5, (char)97, t12, t17, (char)101);
    t11 = (28U + 8U);
    t21 = (t11 + 4U);
    t22 = (40U != t21);
    if (t22 == 1)
        goto LAB5;

LAB6:    t19 = (t0 + 6992);
    t23 = (t19 + 56U);
    t24 = *((char **)t23);
    t25 = (t24 + 56U);
    t26 = *((char **)t25);
    memcpy(t26, t14, 40U);
    xsi_driver_first_trans_fast(t19);

LAB2:    t27 = (t0 + 6752);
    *((int *)t27) = 1;

LAB1:    return;
LAB4:    goto LAB2;

LAB5:    xsi_size_not_matching(40U, t21, 0);
    goto LAB6;

}

static void work_a_1475831506_3212880686_p_1(char *t0)
{
    char t5[16];
    char t7[16];
    char t15[16];
    char t17[16];
    char *t1;
    char *t3;
    char *t4;
    char *t6;
    char *t8;
    char *t9;
    int t10;
    unsigned int t11;
    char *t12;
    char *t14;
    char *t16;
    char *t18;
    char *t19;
    int t20;
    unsigned int t21;
    unsigned char t22;
    char *t23;
    char *t24;
    char *t25;
    char *t26;
    char *t27;

LAB0:    xsi_set_current_line(19, ng0);

LAB3:    t1 = (t0 + 10192);
    t3 = (t0 + 1032U);
    t4 = *((char **)t3);
    t6 = ((IEEE_P_3499444699) + 2616);
    t8 = (t7 + 0U);
    t9 = (t8 + 0U);
    *((int *)t9) = 0;
    t9 = (t8 + 4U);
    *((int *)t9) = 25;
    t9 = (t8 + 8U);
    *((int *)t9) = 1;
    t10 = (25 - 0);
    t11 = (t10 * 1);
    t11 = (t11 + 1);
    t9 = (t8 + 12U);
    *((unsigned int *)t9) = t11;
    t9 = (t0 + 9696U);
    t3 = xsi_base_array_concat(t3, t5, t6, (char)97, t1, t7, (char)97, t4, t9, (char)101);
    t12 = (t0 + 10218);
    t16 = ((IEEE_P_3499444699) + 2616);
    t18 = (t17 + 0U);
    t19 = (t18 + 0U);
    *((int *)t19) = 0;
    t19 = (t18 + 4U);
    *((int *)t19) = 5;
    t19 = (t18 + 8U);
    *((int *)t19) = 1;
    t20 = (5 - 0);
    t11 = (t20 * 1);
    t11 = (t11 + 1);
    t19 = (t18 + 12U);
    *((unsigned int *)t19) = t11;
    t14 = xsi_base_array_concat(t14, t15, t16, (char)97, t3, t5, (char)97, t12, t17, (char)101);
    t11 = (26U + 8U);
    t21 = (t11 + 6U);
    t22 = (40U != t21);
    if (t22 == 1)
        goto LAB5;

LAB6:    t19 = (t0 + 7056);
    t23 = (t19 + 56U);
    t24 = *((char **)t23);
    t25 = (t24 + 56U);
    t26 = *((char **)t25);
    memcpy(t26, t14, 40U);
    xsi_driver_first_trans_fast(t19);

LAB2:    t27 = (t0 + 6768);
    *((int *)t27) = 1;

LAB1:    return;
LAB4:    goto LAB2;

LAB5:    xsi_size_not_matching(40U, t21, 0);
    goto LAB6;

}

static void work_a_1475831506_3212880686_p_2(char *t0)
{
    char t5[16];
    char t7[16];
    char t15[16];
    char t17[16];
    char *t1;
    char *t3;
    char *t4;
    char *t6;
    char *t8;
    char *t9;
    int t10;
    unsigned int t11;
    char *t12;
    char *t14;
    char *t16;
    char *t18;
    char *t19;
    int t20;
    unsigned int t21;
    unsigned char t22;
    char *t23;
    char *t24;
    char *t25;
    char *t26;
    char *t27;

LAB0:    xsi_set_current_line(23, ng0);

LAB3:    t1 = (t0 + 10224);
    t3 = (t0 + 1032U);
    t4 = *((char **)t3);
    t6 = ((IEEE_P_3499444699) + 2616);
    t8 = (t7 + 0U);
    t9 = (t8 + 0U);
    *((int *)t9) = 0;
    t9 = (t8 + 4U);
    *((int *)t9) = 21;
    t9 = (t8 + 8U);
    *((int *)t9) = 1;
    t10 = (21 - 0);
    t11 = (t10 * 1);
    t11 = (t11 + 1);
    t9 = (t8 + 12U);
    *((unsigned int *)t9) = t11;
    t9 = (t0 + 9696U);
    t3 = xsi_base_array_concat(t3, t5, t6, (char)97, t1, t7, (char)97, t4, t9, (char)101);
    t12 = (t0 + 10246);
    t16 = ((IEEE_P_3499444699) + 2616);
    t18 = (t17 + 0U);
    t19 = (t18 + 0U);
    *((int *)t19) = 0;
    t19 = (t18 + 4U);
    *((int *)t19) = 9;
    t19 = (t18 + 8U);
    *((int *)t19) = 1;
    t20 = (9 - 0);
    t11 = (t20 * 1);
    t11 = (t11 + 1);
    t19 = (t18 + 12U);
    *((unsigned int *)t19) = t11;
    t14 = xsi_base_array_concat(t14, t15, t16, (char)97, t3, t5, (char)97, t12, t17, (char)101);
    t11 = (22U + 8U);
    t21 = (t11 + 10U);
    t22 = (40U != t21);
    if (t22 == 1)
        goto LAB5;

LAB6:    t19 = (t0 + 7120);
    t23 = (t19 + 56U);
    t24 = *((char **)t23);
    t25 = (t24 + 56U);
    t26 = *((char **)t25);
    memcpy(t26, t14, 40U);
    xsi_driver_first_trans_fast(t19);

LAB2:    t27 = (t0 + 6784);
    *((int *)t27) = 1;

LAB1:    return;
LAB4:    goto LAB2;

LAB5:    xsi_size_not_matching(40U, t21, 0);
    goto LAB6;

}

static void work_a_1475831506_3212880686_p_3(char *t0)
{
    char t5[16];
    char t7[16];
    char t15[16];
    char t17[16];
    char *t1;
    char *t3;
    char *t4;
    char *t6;
    char *t8;
    char *t9;
    int t10;
    unsigned int t11;
    char *t12;
    char *t14;
    char *t16;
    char *t18;
    char *t19;
    int t20;
    unsigned int t21;
    unsigned char t22;
    char *t23;
    char *t24;
    char *t25;
    char *t26;
    char *t27;

LAB0:    xsi_set_current_line(25, ng0);

LAB3:    t1 = (t0 + 10256);
    t3 = (t0 + 1032U);
    t4 = *((char **)t3);
    t6 = ((IEEE_P_3499444699) + 2616);
    t8 = (t7 + 0U);
    t9 = (t8 + 0U);
    *((int *)t9) = 0;
    t9 = (t8 + 4U);
    *((int *)t9) = 19;
    t9 = (t8 + 8U);
    *((int *)t9) = 1;
    t10 = (19 - 0);
    t11 = (t10 * 1);
    t11 = (t11 + 1);
    t9 = (t8 + 12U);
    *((unsigned int *)t9) = t11;
    t9 = (t0 + 9696U);
    t3 = xsi_base_array_concat(t3, t5, t6, (char)97, t1, t7, (char)97, t4, t9, (char)101);
    t12 = (t0 + 10276);
    t16 = ((IEEE_P_3499444699) + 2616);
    t18 = (t17 + 0U);
    t19 = (t18 + 0U);
    *((int *)t19) = 0;
    t19 = (t18 + 4U);
    *((int *)t19) = 11;
    t19 = (t18 + 8U);
    *((int *)t19) = 1;
    t20 = (11 - 0);
    t11 = (t20 * 1);
    t11 = (t11 + 1);
    t19 = (t18 + 12U);
    *((unsigned int *)t19) = t11;
    t14 = xsi_base_array_concat(t14, t15, t16, (char)97, t3, t5, (char)97, t12, t17, (char)101);
    t11 = (20U + 8U);
    t21 = (t11 + 12U);
    t22 = (40U != t21);
    if (t22 == 1)
        goto LAB5;

LAB6:    t19 = (t0 + 7184);
    t23 = (t19 + 56U);
    t24 = *((char **)t23);
    t25 = (t24 + 56U);
    t26 = *((char **)t25);
    memcpy(t26, t14, 40U);
    xsi_driver_first_trans_fast(t19);

LAB2:    t27 = (t0 + 6800);
    *((int *)t27) = 1;

LAB1:    return;
LAB4:    goto LAB2;

LAB5:    xsi_size_not_matching(40U, t21, 0);
    goto LAB6;

}

static void work_a_1475831506_3212880686_p_4(char *t0)
{
    char t5[16];
    char t7[16];
    char t15[16];
    char t17[16];
    char *t1;
    char *t3;
    char *t4;
    char *t6;
    char *t8;
    char *t9;
    int t10;
    unsigned int t11;
    char *t12;
    char *t14;
    char *t16;
    char *t18;
    char *t19;
    int t20;
    unsigned int t21;
    unsigned char t22;
    char *t23;
    char *t24;
    char *t25;
    char *t26;
    char *t27;

LAB0:    xsi_set_current_line(26, ng0);

LAB3:    t1 = (t0 + 10288);
    t3 = (t0 + 1032U);
    t4 = *((char **)t3);
    t6 = ((IEEE_P_3499444699) + 2616);
    t8 = (t7 + 0U);
    t9 = (t8 + 0U);
    *((int *)t9) = 0;
    t9 = (t8 + 4U);
    *((int *)t9) = 18;
    t9 = (t8 + 8U);
    *((int *)t9) = 1;
    t10 = (18 - 0);
    t11 = (t10 * 1);
    t11 = (t11 + 1);
    t9 = (t8 + 12U);
    *((unsigned int *)t9) = t11;
    t9 = (t0 + 9696U);
    t3 = xsi_base_array_concat(t3, t5, t6, (char)97, t1, t7, (char)97, t4, t9, (char)101);
    t12 = (t0 + 10307);
    t16 = ((IEEE_P_3499444699) + 2616);
    t18 = (t17 + 0U);
    t19 = (t18 + 0U);
    *((int *)t19) = 0;
    t19 = (t18 + 4U);
    *((int *)t19) = 12;
    t19 = (t18 + 8U);
    *((int *)t19) = 1;
    t20 = (12 - 0);
    t11 = (t20 * 1);
    t11 = (t11 + 1);
    t19 = (t18 + 12U);
    *((unsigned int *)t19) = t11;
    t14 = xsi_base_array_concat(t14, t15, t16, (char)97, t3, t5, (char)97, t12, t17, (char)101);
    t11 = (19U + 8U);
    t21 = (t11 + 13U);
    t22 = (40U != t21);
    if (t22 == 1)
        goto LAB5;

LAB6:    t19 = (t0 + 7248);
    t23 = (t19 + 56U);
    t24 = *((char **)t23);
    t25 = (t24 + 56U);
    t26 = *((char **)t25);
    memcpy(t26, t14, 40U);
    xsi_driver_first_trans_fast(t19);

LAB2:    t27 = (t0 + 6816);
    *((int *)t27) = 1;

LAB1:    return;
LAB4:    goto LAB2;

LAB5:    xsi_size_not_matching(40U, t21, 0);
    goto LAB6;

}

static void work_a_1475831506_3212880686_p_5(char *t0)
{
    char t5[16];
    char t7[16];
    char t15[16];
    char t17[16];
    char *t1;
    char *t3;
    char *t4;
    char *t6;
    char *t8;
    char *t9;
    int t10;
    unsigned int t11;
    char *t12;
    char *t14;
    char *t16;
    char *t18;
    char *t19;
    int t20;
    unsigned int t21;
    unsigned char t22;
    char *t23;
    char *t24;
    char *t25;
    char *t26;
    char *t27;

LAB0:    xsi_set_current_line(28, ng0);

LAB3:    t1 = (t0 + 10320);
    t3 = (t0 + 1032U);
    t4 = *((char **)t3);
    t6 = ((IEEE_P_3499444699) + 2616);
    t8 = (t7 + 0U);
    t9 = (t8 + 0U);
    *((int *)t9) = 0;
    t9 = (t8 + 4U);
    *((int *)t9) = 16;
    t9 = (t8 + 8U);
    *((int *)t9) = 1;
    t10 = (16 - 0);
    t11 = (t10 * 1);
    t11 = (t11 + 1);
    t9 = (t8 + 12U);
    *((unsigned int *)t9) = t11;
    t9 = (t0 + 9696U);
    t3 = xsi_base_array_concat(t3, t5, t6, (char)97, t1, t7, (char)97, t4, t9, (char)101);
    t12 = (t0 + 10337);
    t16 = ((IEEE_P_3499444699) + 2616);
    t18 = (t17 + 0U);
    t19 = (t18 + 0U);
    *((int *)t19) = 0;
    t19 = (t18 + 4U);
    *((int *)t19) = 14;
    t19 = (t18 + 8U);
    *((int *)t19) = 1;
    t20 = (14 - 0);
    t11 = (t20 * 1);
    t11 = (t11 + 1);
    t19 = (t18 + 12U);
    *((unsigned int *)t19) = t11;
    t14 = xsi_base_array_concat(t14, t15, t16, (char)97, t3, t5, (char)97, t12, t17, (char)101);
    t11 = (17U + 8U);
    t21 = (t11 + 15U);
    t22 = (40U != t21);
    if (t22 == 1)
        goto LAB5;

LAB6:    t19 = (t0 + 7312);
    t23 = (t19 + 56U);
    t24 = *((char **)t23);
    t25 = (t24 + 56U);
    t26 = *((char **)t25);
    memcpy(t26, t14, 40U);
    xsi_driver_first_trans_fast(t19);

LAB2:    t27 = (t0 + 6832);
    *((int *)t27) = 1;

LAB1:    return;
LAB4:    goto LAB2;

LAB5:    xsi_size_not_matching(40U, t21, 0);
    goto LAB6;

}

static void work_a_1475831506_3212880686_p_6(char *t0)
{
    char t5[16];
    char t7[16];
    char t15[16];
    char t17[16];
    char *t1;
    char *t3;
    char *t4;
    char *t6;
    char *t8;
    char *t9;
    int t10;
    unsigned int t11;
    char *t12;
    char *t14;
    char *t16;
    char *t18;
    char *t19;
    int t20;
    unsigned int t21;
    unsigned char t22;
    char *t23;
    char *t24;
    char *t25;
    char *t26;
    char *t27;

LAB0:    xsi_set_current_line(35, ng0);

LAB3:    t1 = (t0 + 10352);
    t3 = (t0 + 1032U);
    t4 = *((char **)t3);
    t6 = ((IEEE_P_3499444699) + 2616);
    t8 = (t7 + 0U);
    t9 = (t8 + 0U);
    *((int *)t9) = 0;
    t9 = (t8 + 4U);
    *((int *)t9) = 9;
    t9 = (t8 + 8U);
    *((int *)t9) = 1;
    t10 = (9 - 0);
    t11 = (t10 * 1);
    t11 = (t11 + 1);
    t9 = (t8 + 12U);
    *((unsigned int *)t9) = t11;
    t9 = (t0 + 9696U);
    t3 = xsi_base_array_concat(t3, t5, t6, (char)97, t1, t7, (char)97, t4, t9, (char)101);
    t12 = (t0 + 10362);
    t16 = ((IEEE_P_3499444699) + 2616);
    t18 = (t17 + 0U);
    t19 = (t18 + 0U);
    *((int *)t19) = 0;
    t19 = (t18 + 4U);
    *((int *)t19) = 21;
    t19 = (t18 + 8U);
    *((int *)t19) = 1;
    t20 = (21 - 0);
    t11 = (t20 * 1);
    t11 = (t11 + 1);
    t19 = (t18 + 12U);
    *((unsigned int *)t19) = t11;
    t14 = xsi_base_array_concat(t14, t15, t16, (char)97, t3, t5, (char)97, t12, t17, (char)101);
    t11 = (10U + 8U);
    t21 = (t11 + 22U);
    t22 = (40U != t21);
    if (t22 == 1)
        goto LAB5;

LAB6:    t19 = (t0 + 7376);
    t23 = (t19 + 56U);
    t24 = *((char **)t23);
    t25 = (t24 + 56U);
    t26 = *((char **)t25);
    memcpy(t26, t14, 40U);
    xsi_driver_first_trans_fast(t19);

LAB2:    t27 = (t0 + 6848);
    *((int *)t27) = 1;

LAB1:    return;
LAB4:    goto LAB2;

LAB5:    xsi_size_not_matching(40U, t21, 0);
    goto LAB6;

}

static void work_a_1475831506_3212880686_p_7(char *t0)
{
    char t5[16];
    char t7[16];
    char t15[16];
    char t17[16];
    char *t1;
    char *t3;
    char *t4;
    char *t6;
    char *t8;
    char *t9;
    int t10;
    unsigned int t11;
    char *t12;
    char *t14;
    char *t16;
    char *t18;
    char *t19;
    int t20;
    unsigned int t21;
    unsigned char t22;
    char *t23;
    char *t24;
    char *t25;
    char *t26;
    char *t27;

LAB0:    xsi_set_current_line(37, ng0);

LAB3:    t1 = (t0 + 10384);
    t3 = (t0 + 1032U);
    t4 = *((char **)t3);
    t6 = ((IEEE_P_3499444699) + 2616);
    t8 = (t7 + 0U);
    t9 = (t8 + 0U);
    *((int *)t9) = 0;
    t9 = (t8 + 4U);
    *((int *)t9) = 7;
    t9 = (t8 + 8U);
    *((int *)t9) = 1;
    t10 = (7 - 0);
    t11 = (t10 * 1);
    t11 = (t11 + 1);
    t9 = (t8 + 12U);
    *((unsigned int *)t9) = t11;
    t9 = (t0 + 9696U);
    t3 = xsi_base_array_concat(t3, t5, t6, (char)97, t1, t7, (char)97, t4, t9, (char)101);
    t12 = (t0 + 10392);
    t16 = ((IEEE_P_3499444699) + 2616);
    t18 = (t17 + 0U);
    t19 = (t18 + 0U);
    *((int *)t19) = 0;
    t19 = (t18 + 4U);
    *((int *)t19) = 23;
    t19 = (t18 + 8U);
    *((int *)t19) = 1;
    t20 = (23 - 0);
    t11 = (t20 * 1);
    t11 = (t11 + 1);
    t19 = (t18 + 12U);
    *((unsigned int *)t19) = t11;
    t14 = xsi_base_array_concat(t14, t15, t16, (char)97, t3, t5, (char)97, t12, t17, (char)101);
    t11 = (8U + 8U);
    t21 = (t11 + 24U);
    t22 = (40U != t21);
    if (t22 == 1)
        goto LAB5;

LAB6:    t19 = (t0 + 7440);
    t23 = (t19 + 56U);
    t24 = *((char **)t23);
    t25 = (t24 + 56U);
    t26 = *((char **)t25);
    memcpy(t26, t14, 40U);
    xsi_driver_first_trans_fast(t19);

LAB2:    t27 = (t0 + 6864);
    *((int *)t27) = 1;

LAB1:    return;
LAB4:    goto LAB2;

LAB5:    xsi_size_not_matching(40U, t21, 0);
    goto LAB6;

}

static void work_a_1475831506_3212880686_p_8(char *t0)
{
    char t5[16];
    char t8[16];
    char *t1;
    char *t2;
    char *t4;
    char *t6;
    char *t7;
    char *t9;
    char *t10;
    int t11;
    unsigned int t12;
    unsigned char t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;

LAB0:    xsi_set_current_line(44, ng0);

LAB3:    t1 = (t0 + 1032U);
    t2 = *((char **)t1);
    t1 = (t0 + 10416);
    t6 = ((IEEE_P_3499444699) + 2616);
    t7 = (t0 + 9696U);
    t9 = (t8 + 0U);
    t10 = (t9 + 0U);
    *((int *)t10) = 0;
    t10 = (t9 + 4U);
    *((int *)t10) = 31;
    t10 = (t9 + 8U);
    *((int *)t10) = 1;
    t11 = (31 - 0);
    t12 = (t11 * 1);
    t12 = (t12 + 1);
    t10 = (t9 + 12U);
    *((unsigned int *)t10) = t12;
    t4 = xsi_base_array_concat(t4, t5, t6, (char)97, t2, t7, (char)97, t1, t8, (char)101);
    t12 = (8U + 32U);
    t13 = (40U != t12);
    if (t13 == 1)
        goto LAB5;

LAB6:    t10 = (t0 + 7504);
    t14 = (t10 + 56U);
    t15 = *((char **)t14);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    memcpy(t17, t4, 40U);
    xsi_driver_first_trans_fast(t10);

LAB2:    t18 = (t0 + 6880);
    *((int *)t18) = 1;

LAB1:    return;
LAB4:    goto LAB2;

LAB5:    xsi_size_not_matching(40U, t12, 0);
    goto LAB6;

}

static void work_a_1475831506_3212880686_p_9(char *t0)
{
    char t1[16];
    char t2[16];
    char t3[16];
    char t4[16];
    char t5[16];
    char t6[16];
    char t7[16];
    char t8[16];
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;
    char *t19;
    char *t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;
    char *t25;
    char *t26;
    char *t27;
    char *t28;
    char *t29;
    char *t30;
    char *t31;
    char *t32;
    char *t33;
    char *t34;
    char *t35;
    unsigned int t36;
    unsigned int t37;
    unsigned char t38;
    char *t39;
    char *t40;
    char *t41;
    char *t42;
    char *t43;
    char *t44;

LAB0:    xsi_set_current_line(45, ng0);

LAB3:    t9 = (t0 + 1512U);
    t10 = *((char **)t9);
    t9 = (t0 + 9728U);
    t11 = (t0 + 1672U);
    t12 = *((char **)t11);
    t11 = (t0 + 9728U);
    t13 = ieee_p_3499444699_sub_2254111597_3536714472(IEEE_P_3499444699, t8, t10, t9, t12, t11);
    t14 = (t0 + 1832U);
    t15 = *((char **)t14);
    t14 = (t0 + 9728U);
    t16 = ieee_p_3499444699_sub_2254111597_3536714472(IEEE_P_3499444699, t7, t13, t8, t15, t14);
    t17 = (t0 + 1992U);
    t18 = *((char **)t17);
    t17 = (t0 + 9728U);
    t19 = ieee_p_3499444699_sub_2254111597_3536714472(IEEE_P_3499444699, t6, t16, t7, t18, t17);
    t20 = (t0 + 2152U);
    t21 = *((char **)t20);
    t20 = (t0 + 9728U);
    t22 = ieee_p_3499444699_sub_2254111597_3536714472(IEEE_P_3499444699, t5, t19, t6, t21, t20);
    t23 = (t0 + 2472U);
    t24 = *((char **)t23);
    t23 = (t0 + 9728U);
    t25 = ieee_p_3499444699_sub_2254111597_3536714472(IEEE_P_3499444699, t4, t22, t5, t24, t23);
    t26 = (t0 + 2792U);
    t27 = *((char **)t26);
    t26 = (t0 + 9728U);
    t28 = ieee_p_3499444699_sub_2254111597_3536714472(IEEE_P_3499444699, t3, t25, t4, t27, t26);
    t29 = (t0 + 2632U);
    t30 = *((char **)t29);
    t29 = (t0 + 9728U);
    t31 = ieee_p_3499444699_sub_2254183471_3536714472(IEEE_P_3499444699, t2, t28, t3, t30, t29);
    t32 = (t0 + 2312U);
    t33 = *((char **)t32);
    t32 = (t0 + 9728U);
    t34 = ieee_p_3499444699_sub_2254183471_3536714472(IEEE_P_3499444699, t1, t31, t2, t33, t32);
    t35 = (t1 + 12U);
    t36 = *((unsigned int *)t35);
    t37 = (1U * t36);
    t38 = (40U != t37);
    if (t38 == 1)
        goto LAB5;

LAB6:    t39 = (t0 + 7568);
    t40 = (t39 + 56U);
    t41 = *((char **)t40);
    t42 = (t41 + 56U);
    t43 = *((char **)t42);
    memcpy(t43, t34, 40U);
    xsi_driver_first_trans_fast(t39);

LAB2:    t44 = (t0 + 6896);
    *((int *)t44) = 1;

LAB1:    return;
LAB4:    goto LAB2;

LAB5:    xsi_size_not_matching(40U, t37, 0);
    goto LAB6;

}

static void work_a_1475831506_3212880686_p_10(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;

LAB0:    xsi_set_current_line(46, ng0);

LAB3:    t1 = (t0 + 1352U);
    t2 = *((char **)t1);
    t1 = (t0 + 7632);
    t3 = (t1 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    memcpy(t6, t2, 40U);
    xsi_driver_first_trans_fast_port(t1);

LAB2:    t7 = (t0 + 6912);
    *((int *)t7) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}


extern void work_a_1475831506_3212880686_init()
{
	static char *pe[] = {(void *)work_a_1475831506_3212880686_p_0,(void *)work_a_1475831506_3212880686_p_1,(void *)work_a_1475831506_3212880686_p_2,(void *)work_a_1475831506_3212880686_p_3,(void *)work_a_1475831506_3212880686_p_4,(void *)work_a_1475831506_3212880686_p_5,(void *)work_a_1475831506_3212880686_p_6,(void *)work_a_1475831506_3212880686_p_7,(void *)work_a_1475831506_3212880686_p_8,(void *)work_a_1475831506_3212880686_p_9,(void *)work_a_1475831506_3212880686_p_10};
	xsi_register_didat("work_a_1475831506_3212880686", "isim/MRI_sch_MRI_sch_sch_tb_isim_beh.exe.sim/work/a_1475831506_3212880686.didat");
	xsi_register_executes(pe);
}
